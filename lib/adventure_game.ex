defmodule AdventureGame do
  @moduledoc """
  Documentation for AdventureGame.
  """

  @doc """
  Hello world.

  ## Examples

      iex> AdventureGame.hello()
      :world

  """
  def hello do
    :world
  end

  def questions do
    %{
      "how old are you?" => [1..100 |> Enum.to_list()],
      "gender?" => ["m","male","f","female"],
      "what is your favorite color?" => ["red","green","blue"],
    }
  end

  def rndquestion do
    Enum.random(questions())
  end

  def askquestion do

    {q, list_of_answers} = rndquestion

    response =
      IO.gets(q)
      |> String.trim()


    case q do
      "gender?" -> gender_handler(list_of_answers,response)
      # "how old are you?" -> "#{response}"
      person  ->
          IO.puts "Hi #{person}"
          askquestion()

    end

  end

  def gender_handler(list,response) do
    case Enum.find(list,fn(ans) -> response == ans end) do
      "m" -> "hey man!"
      _ ->
        "error, did not match expected response"
        askquestion()
    end
  end
end
