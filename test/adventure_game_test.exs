defmodule AdventureGameTest do
  use ExUnit.Case
  doctest AdventureGame

  test "greets the world" do
    assert AdventureGame.hello() == :world
  end

  test "returns a list of questions" do
    qs = ["how old are you?",
    "gender?","what is your favorite color?"]

    assert AdventureGame.questions() == qs
  end

  test "ask a random question" do
    qs = ["how old are you?",
    "gender?","what is your favorite color?"]
    # capture.io("input", fn ->
    #   prompt = IO.gets "> {}"
    #   IO.write prompt
    # end)
    assert AdventureGame.rndquestion() in qs
  end
end
